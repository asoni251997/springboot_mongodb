package com.User.Management.Controller;

import java.util.List;

import org.bson.types.ObjectId;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.User.Management.model.User;
import com.User.Management.userRepository.UserRepository;

@RestController
@RequestMapping("/users")
public class UserController {

	@Autowired
	private UserRepository userRepository;
	
	
	@RequestMapping(value = "/getAllUsers", method = RequestMethod.GET)
	public List<User> getAllUsers(){
		return userRepository.findAll();
	}
	
	@RequestMapping(value = "/getUserbyId/{id}", method = RequestMethod.GET)
	public User getUserbyId(@PathVariable("id") ObjectId id) {
		return userRepository.findBy_id(id);
	}
	
	@RequestMapping(value = "/updateUser/{id}", method = RequestMethod.PUT)
	public void modifyUserById(@PathVariable("id") ObjectId id, @Validated 
	@RequestBody User user) {
	  user.set_id(id);
	  userRepository.save(user);
	}
	
	@RequestMapping(value = "/createUser", method = RequestMethod.POST)
	public User createUser(@Validated @RequestBody User user) {
		user.set_id(ObjectId.get());
		userRepository.save(user);
		return user;
	} 
	
	public void deleteById(@PathVariable ObjectId id) {
		userRepository.delete(userRepository.findBy_id(id));
	}
}
