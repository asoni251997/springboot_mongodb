package com.User.Management.userRepository;

import org.bson.types.ObjectId;
import org.springframework.data.mongodb.repository.MongoRepository;

import com.User.Management.model.User;

public interface UserRepository extends MongoRepository<User, String> {

	User findBy_id(ObjectId _id);
}
