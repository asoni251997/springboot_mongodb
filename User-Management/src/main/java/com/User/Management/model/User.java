package com.User.Management.model;

import org.bson.types.ObjectId;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;


@Document(collection = "user")
public class User {

	@Id
	public ObjectId _id;
	
	public String firstname;
	public String lastname;
	public String address;
	
	public User() {}
	
	public User(ObjectId _id, String firstname, String lastname, String address) {
		super();
		this._id = _id;
		this.firstname = firstname;
		this.lastname = lastname;
		this.address = address;
	}

	public String get_id() {
		return _id.toHexString();
	}

	public void set_id(ObjectId _id) {
		this._id = _id;
	}

	public String getFirstname() {
		return firstname;
	}

	public void setFirstname(String firstname) {
		this.firstname = firstname;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}
	
	
	
}
